from base64 import b64encode
from datetime import date, timedelta, time, datetime
from io import BytesIO, StringIO

from flask import (
    Flask, 
    render_template, 
    jsonify, 
    request,
    redirect,
    url_for,
    session,
    g
)

# from flask_debugtoolbar import DebugToolbarExtension

import pandas as pd
import numpy as np

HOUR_DELTA = 60 * 60

app = Flask(__name__, static_url_path="", static_folder="static")
app.debug = False
# app.config['SECRET_KEY'] = 'akjij3i090ijgsfaifpsjiasjifj020990JSLDKjfladsjfajsdljdfsa'
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

# toolbar = DebugToolbarExtension(app)

SECRET_TOKEN = 'JDiMaRRsD8DIij32j2300'
SLEEP_START = time(19, 0)
SLEEP_END = time(8, 0)
MIDNIGHT = time(0, 0)

def is_morning():
    now = datetime.now()
    return now.time() >= MIDNIGHT and now.time() <= time(12,0)

def is_evening():
    now = datetime.now()
    return now.time() >= time(6,0) and now.time() <= MIDNIGHT

def in_sleep_period():
    now = datetime.now()
    return (is_morning() and now.time() <= SLEEP_END) or (is_evening() and now.time >= SLEEP_START)

def sleep_start_yesterday():
    days_delta = 1
    if in_sleep_period() and is_morning():
        days_delta = 2
        
    return datetime.combine((datetime.now() - timedelta(days=days_delta)).date(), SLEEP_START)

def sleep_start_today():
    days_delta = 1
    if in_sleep_period() and is_evening():
        days_delta = 0
    
    return datetime.combine((datetime.now() - timedelta(days=days_delta)).date(), SLEEP_START)

def waking_event_sleep_resampler(wakings, as_dict=False):
    """
    Takes all waking time series events and computes the count for each period.
    A sleep period is defined by the variables SLEEP_START and SLEEP_END. The periods
    are windowed over two days and fixed on both sides. Dates by this function 
    are the start of the sleep window.

    I tried to use pandas.resample with BusinessHour, however there is currently a bug. 
    This is a work around for now.
    """
    start = None
    count = 0

    resampled = {
        'date': [],
        'count': [],
    }

    if as_dict:
        resampled = {}

    def get_start_datetime(event_time):
        start_datetime = datetime.combine(event_time.date(), SLEEP_START)
        if event_time.time() >= MIDNIGHT and event_time.time() <= SLEEP_END:
            start_datetime = datetime.combine((event_time - timedelta(days=1)).date(), SLEEP_START)
        
        return start_datetime
    
    for event_time in wakings.index.to_pydatetime():
        if start is None:
            start = get_start_datetime(event_time)
        elif (event_time - start).total_seconds() / HOUR_DELTA >= 11:
            if as_dict:
                resampled[start.date()] = count
            else:
                resampled['date'].append(start.date())
                resampled['count'].append(count)

            start = get_start_datetime(event_time)
            count = 0
        
        count += 1
    
    if as_dict:
        resampled[start.date()] = count
    else:
        resampled['date'].append(start.date())
        resampled['count'].append(count)
    
    return resampled

def is_day_time(t):
    is_day_time = False
    day_start = time(6, 0)
    day_end = time(20, 0)
    
    if t >= day_start and t <= day_end:
        is_day_time = True
    
    return is_day_time

def day_count_stats(df, when, prefix):
    when_filter = df['datetime'].dt.date == when
    counts = df[when_filter].groupby('event').size()
    data = {}

    with open('activities.txt') as f:
        for activity in f:
            activity = activity.strip()

            if activity == 'Waking':
                continue

            result = 0
            try:
                result = int(counts.loc[activity])
            except:
                pass

            data[prefix + activity + 'Count'] = result

    return data

def waking_count_stats(df, when, prefix):
    now = datetime.now()
    in_sleep_period = now.time() >= SLEEP_START and now.time <= SLEEP_END

    if when == 'today':
        pass
    elif when == 'yesterday':
        pass

def time_differential_stats(df):
    result = {}
    pivoted = df.pivot(columns='event', values='datetime')

    with open('activities.txt') as f:
        for activity in f:
            activity = activity.strip()
            try:
                values = pivoted[activity].dropna().values
                diffs = np.diff(values.astype('datetime64[s]').astype('int64'))
                result[activity.lower() + 'MeanDifferential'] = round(np.mean(diffs) / HOUR_DELTA, 2)
                result[activity.lower() + 'MedianDifferential'] = round(np.median(diffs) / HOUR_DELTA, 2)
            except:
                result[activity.lower() + 'MeanDifferential'] = 0
                result[activity.lower() + 'MedianDifferential'] = 0
    
    return result

def event_stats(df):

    lasts = {}

    with open('activities.txt') as f:
        for activity in f:
            activity = activity.strip()
            try:
                lasts['last' + activity] = round(df[df['event'] == activity].tail(1)['timesince'].values[0] / np.timedelta64(1, 'h'), 2)
            except:
                lasts['last' + activity] = 0

    today = date.today()
    today_counts = day_count_stats(df, today, 'today')

    yesterday = today - timedelta(days=1)
    yesterday_counts = day_count_stats(df, yesterday, 'yesterday')

    time_differentials = time_differential_stats(df)

    # handle Waking event today and yesterday counts differently
    wakings_df = df[df['event'] == 'Waking'].set_index('datetime').sort_index()
    wakings_resampled = waking_event_sleep_resampler(wakings_df, as_dict=True)
    yesterday = sleep_start_yesterday()
    today = sleep_start_today()
    waking_counts = {
        'yesterdayWakingCount': wakings_resampled.get(yesterday.date(), 0),
        'todayWakingCount': wakings_resampled.get(today.date(), 0),
    }


    return {**lasts, **today_counts, **yesterday_counts, **time_differentials, **waking_counts}

def preprocess_df():
    df = pd.read_csv('data.csv', names=['event', 'datetime'])
    df['datetime'] = pd.to_datetime(df['datetime'])
    df['date'] = df['datetime'].dt.date
    df = df.sort_values('datetime')
    df['timesince'] = pd.Timestamp.now(tz='America/Chicago').tz_localize(None) - df['datetime']
    df['day_time'] = df['datetime'].dt.time.apply(is_day_time)

    return df

@app.route("/", methods=['GET'])
def index():
    token = request.args.get('token')
    if token != SECRET_TOKEN:
        return '401 Forbidden'

    activities = []
    with open('activities.txt') as f:
        for activity in f:
            activities.append(activity.strip())

    df = preprocess_df()

    return render_template(
        "index.html",
        activities=activities,
        **event_stats(df)
    )

@app.route("/api/eventstats.json", methods=['GET'])
def api_event_stats():
    return jsonify(event_stats())

@app.route("/events", methods=['GET'])
def events():
    events = []
    with open('data.csv') as f:
        for event in f:
            tmp = event.split(',')
            events.append({
                'activity': tmp[0],
                'eventtime': tmp[1]
            })

    return render_template("events.html", events=events)

@app.route("/api/track", methods=['POST'])
def api_track():
    activity = request.form.get('activity').strip()
    event_time = request.form.get('eventtime').strip()

    with open('data.csv', 'a') as out:
        out.write('{},{}\n'.format(activity, event_time))

    return jsonify({
        'status': 'ok',
        'message': 'Saved {} event at {}'.format(activity, event_time)
    })

@app.route("/api/dailyevents.json", methods=['GET'])
def api_daily_events():
    df = preprocess_df()
    daily_pivot_df = df.groupby(['date', 'event']).size()
    daily_pivot_df = daily_pivot_df.reset_index().rename(index=str, columns={0: 'count'})
    daily_pivot_df = daily_pivot_df.pivot(index='date', columns='event', values='count')

    result = {}
    with open('activities.txt') as f:
        for activity in f:
            activity = activity.strip()
            try:
                if activity == 'Waking':
                    waking_df = df[df['event'] == 'Waking'].set_index('datetime').sort_index()
                    resampled_wakings = waking_event_sleep_resampler(waking_df)
                    result[activity.lower()] = {
                        'x': [str(d) for d in resampled_wakings['date']],
                        'y': resampled_wakings['count']
                    }
                else:
                    result[activity.lower()] = {
                        'x': list(map(lambda d: str(d), daily_pivot_df[activity].dropna().index.tolist())),
                        'y': daily_pivot_df[activity].dropna().values.tolist()
                    }
            except Exception as e:
                result[activity.lower()] = {
                    'x': [],
                    'y': [],
                }

    return jsonify(result)

@app.route("/api/daynighteventpercents.json", methods=['GET'])
def api_day_night_percents():
    df = preprocess_df()
    daytime_df = df.groupby(['event', 'day_time']).size().reset_index().rename(index=str, columns={0: 'count'}).drop_duplicates()
    daytime_df = daytime_df.pivot(index='event', columns='day_time', values='count')
    daytime_df['total'] = daytime_df[True] + daytime_df[False]
    daytime_df['daytime_pct'] = daytime_df[True] / daytime_df['total']
    daytime_df['nighttime_pct'] = daytime_df[False] / daytime_df['total']
    daytime_df = daytime_df[['daytime_pct', 'nighttime_pct']]

    result = {}
    with open('activities.txt') as f:
        for activity in f:
            activity = activity.strip()
            try:
                daytime_pct = round(daytime_df.loc[activity]['daytime_pct'] * 100, 2)
                nighttime_pct = round(daytime_df.loc[activity]['nighttime_pct'] * 100, 2)

                if np.isnan(daytime_pct):
                    daytime_pct = 0

                if np.isnan(nighttime_pct):
                    nighttime_pct = 0

                result[activity.lower()] = {
                    'daytime_pct': daytime_pct,
                    'nighttime_pct': nighttime_pct,
                }
            except:
                result[activity.lower()] = {
                    'daytime_pct': 0,
                    'nighttime_pct': 0,
                }

    return jsonify(result)

@app.route("/api/eventdifferentials.json", methods=['GET'])
def api_event_differentials():
    data = {}
    df = preprocess_df()

    for label, group in df.groupby('event'):
        diffs = np.diff(group['datetime'].values.astype('datetime64[s]').astype('float64')) / (60.0 * 60.0)
        label = label.lower()
        data[label] = {}
        data[label]['y'] = diffs.tolist()
        data[label]['x'] = list(range(0, len(data[label]['y']) + 1))
        
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)