function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function() {
    $('li.activity').click(function(){
    	var activity = $($(this).find('a')[0]).data('activity');
    	var currentDT = moment();
    	$('#modalInfo').html('');
		$('#eventtime').val(currentDT.format('YYYY-MM-DDTHH:mm'));
    	$('#formActivity').val(activity);
    	$('#formActivityLabel').html(activity + ' ' + 'Tracking');
    	$('#myModal').show();
    });

    $('#myModal .close').click(function(){
    	$('#myModal').hide();
    });

    $('#activityform').submit(function(e){
		$.post("/api/track", $("#activityform").serialize(), function(data) {
			if (data.status == 'ok') {
				$('#modalInfo').html('<h2>Success</h2><p>' + data.message + '</p>');
			} else {
				$('#modalInfo').html('<h2>Failed to Save!</h2>');
			}
		});
    	e.preventDefault();
    });

	$.getJSON( "api/dailyevents.json", function( data ) {
		var traces = [];
		$.each(data, function(event, values){
			traces.push({
				x: values.x,
				y: values.y,
				type: 'line',
				name: capitalizeFirstLetter(event)
			})
		});

		var layout = {
			xaxis: {
				type: 'date',
				title: 'Date'
			},
			title: 'Daily Event Counts'
		};

		Plotly.newPlot('dailyeventplot', traces, layout);
	});

	$.getJSON( "/api/daynighteventpercents.json", function( data ) {
		var data = [{
			values: [data.pee.daytime_pct, data.pee.nighttime_pct],
			labels: ['Daytime Pct.', 'Nighttime Pct.'],
			type: 'pie',
			name: 'Pee',
			domain: {
				x: [0, .31],
				y: [0, 0.90]
			},
			hoverinfo: 'label+percent+name',
			},
			{
			values: [data.poop.daytime_pct, data.poop.nighttime_pct],
			labels: ['Daytime Pct.', 'Nighttime Pct.'],
			type: 'pie',
			name: 'Poop',
			domain: {
				x: [0.33, 0.66],
				y: [0, 0.90]
			},
			hoverinfo: 'label+percent+name',
			},
			{
			values: [data.feeding.daytime_pct, data.feeding.nighttime_pct],
			labels: ['Daytime Pct.', 'Nighttime Pct.'],
			type: 'pie',
			name: 'Feeding',
			domain: {
				x: [.68, 1],
				y: [0, 0.90]
			},
			hoverinfo: 'label+percent+name',
		}];

		var layout = {
			title: 'Lifetime Night vs Day Events<br>Daytime is 6AM to 8PM',
			annotations: [{
				font: {
					size: 14
				},
				xanchor: 'center',
				yanchor: 'top',
				showarrow: false,
				text: 'Pee',
				x: (0 + .31) / 2,
				y: 1
			},{
				font: {
					size: 14
				},
				xanchor: 'center',
				yanchor: 'top',
				showarrow: false,
				text: 'Poop',
				x: (.33 + .66) / 2,
				y: 1
			},{
				font: {
					size: 14
				},
				xanchor: 'center',
				yanchor: 'top',
				showarrow: false,
				text: 'Feeding',
				x: (.68 + 1) / 2,
				y: 1
			}]
		};
		Plotly.newPlot('daynightpercentsplot', data, layout);
	});

	// event differential plots
	$.getJSON( "/api/eventdifferentials.json", function( data ) {
		var trace1 = {
		  x: data.pee.x,
		  y: data.pee.y,
		  type: 'line',
		  name: 'Pee'
		};

		var trace2 = {
		  x: data.poop.x,
		  y: data.poop.y,
		  xaxis: 'x2',
		  yaxis: 'y2',
		  type: 'line',
		  name: 'Poop'
		};

		var trace3 = {
		  x: data.feeding.x,
		  y: data.feeding.y,
		  xaxis: 'x3',
		  yaxis: 'y3',
		  type: 'line',
		  name: 'Feeding'
		};

		var data = [trace1, trace2, trace3];

		var layout = {
		  yaxis: {domain: [0, 0.266]},
		  xaxis3: {anchor: 'y3'},
		  xaxis2: {anchor: 'y2'},
		  yaxis2: {domain: [0.366, 0.633]},
		  yaxis3: {domain: [0.733, 1]},
		  title: 'Lifetime Event Differentials (Hours)',
		  legend: {traceorder: 'reversed'}
		};

		Plotly.newPlot('eventdiffplot', data, layout);
	});
});